import 'module-alias/register';
import { ClientFunction, Selector, t } from "testcafe";
import RegisterPage from "model/frontoffice/registration/registration-page";
import helper from 'lib/helper';

const router = require('lib/router');
const help = require('lib/helper');
const getLocation = ClientFunction(() => document.location.href);
const foUrl = router.getFoUrl();

fixture `Registration Chakra`
  .page `${foUrl}/register`
  .meta({
    EPIC: 'Chakra Registration'
  });

const pin = help.getDefaultPassword();
let number = `8${help.randNumber(10)}`

test.skip
  .meta({
    SEVERITY: 'blocker',
    TYPE: 'SMOKE',
    STORY: 'CKR-01'
  })
  (`should be successful register E2E with valid data`, async t => {
    let numberRegist = `8${help.randNumber(10)}`
    await t
      .typeText(RegisterPage.phoneNumber, numberRegist, { paste: true, replace: true })
      .typeText(RegisterPage.referraCode, 'KCAa64', { paste: true, replace: true })
      .click(RegisterPage.btnReferral)
      .click(RegisterPage.termCondition)
      .click(RegisterPage.termText)
      .click(RegisterPage.acceptTermCondition)
      .click(RegisterPage.btnProcessRegister)

      .click(RegisterPage.btnVerifyOtp)
      .typeText(RegisterPage.pinCode, pin)
      .click(RegisterPage.btnNextPin)
      .typeText(RegisterPage.confrimPincode, pin)
      .click(RegisterPage.btnConfirmPin)

      .typeText(RegisterPage.firstName, help.randFisrtName(), { paste: true, replace: true })
      .typeText(RegisterPage.lastName, help.randLastName(), { paste: true, replace: true })
      .typeText(RegisterPage.email, help.randEmail(), { paste: true, replace: true })
      .click(RegisterPage.btnNextRegist)

      .typeText(RegisterPage.address, help.randNamauser(), { paste: true, replace: true })
      .click(RegisterPage.regency)
      .pressKey('j a k').wait(500)
      .pressKey('down')
      .pressKey('down')
      .pressKey('down')
      .pressKey('enter')
      .click(RegisterPage.village)
      .pressKey('down')
      .pressKey('down')
      .pressKey('enter').wait(500)
      .click(RegisterPage.btnNextBirtday)

      .typeText(RegisterPage.BirthDate, help.randDate())
      .click(RegisterPage.btnNextBirtday)

      .click(RegisterPage.genderMale)
      .click(RegisterPage.btnNextGen)
      .expect(getLocation()).contains('/register/complete');
    console.log('number ', numberRegist)
  })

test
  .meta({
    SEVERITY: 'blocker',
    TYPE: 'SMOKE',
    STORY: 'CKR-02'
  })
  (`Initial registration should be success when input valid phone number`, async t => {
    await t
      .typeText(RegisterPage.phoneNumber, `8${help.randNumber(10)}`, { paste: true, replace: true })
      .click(RegisterPage.termCondition)
      .click(RegisterPage.termText)
      .click(RegisterPage.acceptTermCondition)
      .click(RegisterPage.btnProcessRegister)
      .expect(getLocation()).contains('/register/verification-code');
  })

test
  .meta({
    SEVERITY: 'blocker',
    TYPE: 'Negative',
    STORY: 'CKR-03'
  })
  (`Initial registration should be failed when input alfabeth in phone number field`, async t => {
    await t
      .typeText(RegisterPage.phoneNumber, help.randName())
      .click(RegisterPage.btnProcessRegister)
      .expect(RegisterPage.validationMessage.textContent).contains('Phone number is required')
  })

test
  .meta({
    SEVERITY: 'blocker',
    TYPE: 'Negative',
    STORY: 'CKR-04'
  })
  (`should be failed register with invalid number < 10 char`, async t => {
    await t
      .typeText(RegisterPage.phoneNumber, `8${help.randNumber(5)}`, { paste: true, replace: true })
      .click(RegisterPage.btnProcessRegister)
      .expect(RegisterPage.validationMessage.textContent).contains('Must be 8 characters or more')
  })

test
  .meta({
    SEVERITY: 'blocker',
    TYPE: 'Negative',
    STORY: 'CKR-05'
  })
  (`should be failed register without check Terms & Conditions`, async t => {
    await t
      .typeText(RegisterPage.phoneNumber, `8${help.randNumber(10)}`, { paste: true, replace: true })
      .click(RegisterPage.btnProcessRegister)
      .expect(RegisterPage.btnProcessRegister.hasAttribute('disabled')).ok()
  })

test
  .meta({
    SEVERITY: 'blocker',
    TYPE: 'Negative',
    STORY: 'CKR-06'
  })
  (`Initial registration should be failed when input symbols in phone number field`, async t => {
    await t
      .typeText(RegisterPage.phoneNumber, '@#()')
      .click(RegisterPage.btnProcessRegister)
      .expect(RegisterPage.validationMessage.textContent).contains('Phone number is required')
  })

test
  .meta({
    SEVERITY: 'blocker',
    TYPE: 'Negative',
    STORY: 'CKR-07'
  })
  (`Initial registration should be failed when input number and alfabeth in phone number field`, async t => {
    await t
      .typeText(RegisterPage.phoneNumber, help.randAlphanumeric())
      .click(RegisterPage.btnProcessRegister)
      .expect(RegisterPage.validationMessage.textContent).contains('Must be 8 characters or more')
  })

test.skip
  .meta({
    SEVERITY: 'blocker',
    TYPE: 'Negative',
    STORY: 'CKR-08'
  })
  (`Initial registration should be failed when input =!08 number in phone number field`, async t => {
    await t
      .typeText(RegisterPage.phoneNumber, `6${help.randNumber(10)}`, { paste: true, replace: true })
      .click(RegisterPage.termCondition)
      .click(RegisterPage.termText)
      .click(RegisterPage.acceptTermCondition)
      .click(RegisterPage.btnProcessRegister)
      .expect(RegisterPage.validationMessage.textContent).contains('Phone number is not valid')
  })

test
  .meta({
    SEVERITY: 'blocker',
    TYPE: 'Negative',
    STORY: 'CKR-09'
  })
  (`Initial registration should be failed when not input number in phone number field`, async t => {
    await t
      .click(RegisterPage.btnProcessRegister)
      .expect(RegisterPage.btnProcessRegister.hasAttribute('disabled')).ok()
  })

test
  .meta({
    SEVERITY: 'blocker',
    TYPE: 'SMOKE',
    STORY: 'CKR-10'
  })
  (`Initial registration should be success when input valid referral code`, async t => {
    await t
      .typeText(RegisterPage.phoneNumber, `8${help.randNumber(10)}`, { paste: true, replace: true })
      .typeText(RegisterPage.referraCode, 'KCAa64')
      .click(RegisterPage.btnReferral)
      .expect(RegisterPage.successMessageRegist.textContent).contains('Reference code entered successfully')
  })

test
  .meta({
    SEVERITY: 'blocker',
    TYPE: 'Negative',
    STORY: 'CKR-11'
  })
  (`Initial registration should be failed when input invalid referral code`, async t => {
    await t
      .typeText(RegisterPage.phoneNumber, `8${help.randNumber(10)}`, { paste: true, replace: true })
      .typeText(RegisterPage.referraCode, help.randAlphanumeric())
      .click(RegisterPage.btnReferral)
      .expect(RegisterPage.errorMessageregist.textContent).contains('Your referral code is not valid,')
  })

test
  .meta({
    SEVERITY: 'blocker',
    TYPE: 'Negative',
    STORY: 'CKR-12'
  })
  (`Initial registration should be success when not input referral code`, async t => {
    await t
      .typeText(RegisterPage.phoneNumber, `8${help.randNumber(10)}`, { paste: true, replace: true })
      .click(RegisterPage.termCondition)
      .click(RegisterPage.termText)
      .click(RegisterPage.acceptTermCondition)
      .click(RegisterPage.btnProcessRegister)
      .expect(getLocation()).contains('/register/verification-code');
  })

test
  .meta({
    SEVERITY: 'blocker',
    TYPE: 'SMOKE',
    STORY: 'CKR-13'
  })
  (`Input 6-digit pin code should be success to next process`, async t => {
    await t
      .typeText(RegisterPage.phoneNumber, `8${help.randNumber(10)}`, { paste: true, replace: true })
      .click(RegisterPage.termCondition)
      .click(RegisterPage.termText)
      .click(RegisterPage.acceptTermCondition)
      .click(RegisterPage.btnProcessRegister)
      .click(RegisterPage.btnVerifyOtp)
      .typeText(RegisterPage.pinCode, pin)
      .click(RegisterPage.btnNextPin)
      .expect(getLocation()).contains('/register/confirm-pin');
  })

test
  .meta({
    SEVERITY: 'blocker',
    TYPE: 'Negative',
    STORY: 'CKR-14'
  })
  (`Enter <6 digits pin code should not be able to enter the next process`, async t => {
    await t
      .typeText(RegisterPage.phoneNumber, `8${help.randNumber(10)}`, { paste: true, replace: true })
      .click(RegisterPage.termCondition)
      .click(RegisterPage.termText)
      .click(RegisterPage.acceptTermCondition)
      .click(RegisterPage.btnProcessRegister)
      .click(RegisterPage.btnVerifyOtp)
      .typeText(RegisterPage.pinCode, help.randNumber(3))
      .click(RegisterPage.btnNextPin)
      .expect(RegisterPage.btnNextPin.hasAttribute('disabled')).ok()
  })

test
  .meta({
    SEVERITY: 'blocker',
    TYPE: 'Negative',
    STORY: 'CKR-15'
  })
  (`should be failed to next process when not Input digit pin code`, async t => {
    await t
      .typeText(RegisterPage.phoneNumber, `8${help.randNumber(10)}`, { paste: true, replace: true })
      .click(RegisterPage.termCondition)
      .click(RegisterPage.termText)
      .click(RegisterPage.acceptTermCondition)
      .click(RegisterPage.btnProcessRegister)
      .click(RegisterPage.btnVerifyOtp)
      .click(RegisterPage.btnNextPin)
      .expect(RegisterPage.btnNextPin.hasAttribute('disabled')).ok()
  })

test
  .meta({
    SEVERITY: 'blocker',
    TYPE: 'Negative',
    STORY: 'CKR-16'
  })
  (`input digits other than numbers should be fail to be inputted`, async t => {
    await t
      .typeText(RegisterPage.phoneNumber, `8${help.randNumber(10)}`, { paste: true, replace: true })
      .click(RegisterPage.termCondition)
      .click(RegisterPage.termText)
      .click(RegisterPage.acceptTermCondition)
      .click(RegisterPage.btnProcessRegister)
      .click(RegisterPage.btnVerifyOtp)
      .typeText(RegisterPage.pinCode, help.randNamauser())
      .click(RegisterPage.btnNextPin)
      .expect(getLocation()).contains('/register/create-pin');
  })

test
  .meta({
    SEVERITY: 'blocker',
    TYPE: 'Positive',
    STORY: 'CKR-17'
  })
  (`input confirm PIN same with create PIN numbers should be succeess to next process`, async t => {
    await t
      .typeText(RegisterPage.phoneNumber, `8${help.randNumber(10)}`, { paste: true, replace: true })
      .click(RegisterPage.termCondition)
      .click(RegisterPage.termText)
      .click(RegisterPage.acceptTermCondition)
      .click(RegisterPage.btnProcessRegister)
      .click(RegisterPage.btnVerifyOtp)
      .typeText(RegisterPage.pinCode, pin)
      .click(RegisterPage.btnNextPin)
      .typeText(RegisterPage.confrimPincode, pin)
      .click(RegisterPage.btnConfirmPin)
      .expect(getLocation()).contains('/register/basic-account');
  })

test
  .meta({
    SEVERITY: 'blocker',
    TYPE: 'Negative',
    STORY: 'CKR-18'
  })
  (`input confirm PIN not same with create PIN numbers should be failed to next process`, async t => {
    await t
      .typeText(RegisterPage.phoneNumber, `8${help.randNumber(10)}`, { paste: true, replace: true })
      .click(RegisterPage.termCondition)
      .click(RegisterPage.termText)
      .click(RegisterPage.acceptTermCondition)
      .click(RegisterPage.btnProcessRegister)
      .click(RegisterPage.btnVerifyOtp)
      .typeText(RegisterPage.pinCode, pin)
      .click(RegisterPage.btnNextPin)
      .typeText(RegisterPage.confrimPincode, help.randNumber())
      .click(RegisterPage.btnConfirmPin)
      .expect(RegisterPage.alertDanger.textContent).contains('Your PIN not match, please try again.')
  })

test.skip
  .meta({
    SEVERITY: 'blocker',
    TYPE: 'Negative',
    STORY: 'CKR-19'
  })
  (`Input PIN confirmation without a number should be fail to type`, async t => {
    await t
      .typeText(RegisterPage.phoneNumber, number, { paste: true, replace: true })
      .click(RegisterPage.termCondition)
      .click(RegisterPage.termText)
      .click(RegisterPage.acceptTermCondition)
      .click(RegisterPage.btnProcessRegister)
      .click(RegisterPage.btnVerifyOtp)
      .typeText(RegisterPage.pinCode, pin)
      .click(RegisterPage.btnNextPin)
      .typeText(RegisterPage.confrimPincode, help.randDescription())
      .click(RegisterPage.btnConfirmPin)
      .expect(RegisterPage.btnConfirmPin.hasAttribute('disabled')).ok()
    //masih bisa input dan tidak ada validasi
  })

test.skip
  .meta({
    SEVERITY: 'blocker',
    TYPE: 'Negative',
    STORY: 'CKR-20'
  })
  (`Input PIN confirmation with Symbols should be failed next procecss`, async t => {
    await t
      .typeText(RegisterPage.phoneNumber, `8${help.randNumber(10)}`, { paste: true, replace: true })
      .click(RegisterPage.termCondition)
      .click(RegisterPage.termText)
      .click(RegisterPage.acceptTermCondition)
      .click(RegisterPage.btnProcessRegister)
      .click(RegisterPage.btnVerifyOtp)
      .typeText(RegisterPage.pinCode, pin)
      .click(RegisterPage.btnNextPin)
      .typeText(RegisterPage.confrimPincode, help.randEmail())
      .click(RegisterPage.btnConfirmPin)
      .expect(RegisterPage.errorMessageregist.textContent).contains('Pin code must 6 number')
  })

test
  .meta({
    SEVERITY: 'blocker',
    TYPE: 'Negative',
    STORY: 'CKR-21'
  })
  (`input confirm PIN < 6 digit should be failed to next process`, async t => {
    await t
      .typeText(RegisterPage.phoneNumber, `8${help.randNumber(10)}`, { paste: true, replace: true })
      .click(RegisterPage.termCondition)
      .click(RegisterPage.termText)
      .click(RegisterPage.acceptTermCondition)
      .click(RegisterPage.btnProcessRegister)
      .click(RegisterPage.btnVerifyOtp)
      .typeText(RegisterPage.pinCode, pin)
      .click(RegisterPage.btnNextPin)
      .typeText(RegisterPage.confrimPincode, help.randNumber(4))
      .click(RegisterPage.btnConfirmPin)
      .expect(RegisterPage.btnConfirmPin.hasAttribute('disabled')).ok()
  })

test
  .meta({
    SEVERITY: 'blocker',
    TYPE: 'Negative',
    STORY: 'CKR-22'
  })
  (`input verification code with invalid number should be failed to next process and a notification apprears OTP not found or expired`, async t => {
    await t
      .typeText(RegisterPage.phoneNumber, `8${help.randNumber(10)}`, { paste: true, replace: true })
      .click(RegisterPage.termCondition)
      .click(RegisterPage.termText)
      .click(RegisterPage.acceptTermCondition)
      .click(RegisterPage.btnProcessRegister)
      .click(RegisterPage.inputOtp)
      .doubleClick(RegisterPage.inputOtp)
      .pressKey('backspace')
      .typeText(RegisterPage.inputOtp, help.randNumber())
      .click(RegisterPage.btnVerifyOtp)
      .expect(RegisterPage.alertOtp.textContent).contains('OTP not found or expired.')
  })

test.skip
  .meta({
    SEVERITY: 'blocker',
    TYPE: 'Positive',
    STORY: 'CKR-23'
  })
  (`resend verification code should be success received new code`, async t => {
    await t
      .typeText(RegisterPage.phoneNumber, `8${help.randNumber(10)}`, { paste: true, replace: true })
      .click(RegisterPage.termCondition)
      .click(RegisterPage.termText)
      .click(RegisterPage.acceptTermCondition)
      .click(RegisterPage.btnProcessRegister)
      .typeText(RegisterPage.inputOtp, help.randNumber())
      .click(RegisterPage.ResendOtp)
      .expect(RegisterPage.errorMessageregist.textContent).contains('Failed to send otp')
  })

test
  .meta({
    SEVERITY: 'blocker',
    TYPE: 'Positive',
    STORY: 'CKR-24'
  })
  (`input verification code with correct number should be success to next process`, async t => {
    await t
      .typeText(RegisterPage.phoneNumber, `8${help.randNumber(10)}`, { paste: true, replace: true })
      .click(RegisterPage.termCondition)
      .click(RegisterPage.termText)
      .click(RegisterPage.acceptTermCondition)
      .click(RegisterPage.btnProcessRegister)
      .click(RegisterPage.btnVerifyOtp)
      .typeText(RegisterPage.pinCode, pin)
      .click(RegisterPage.btnNextPin)
      .typeText(RegisterPage.confrimPincode, pin)
      .click(RegisterPage.btnConfirmPin)
      .expect(getLocation()).contains('/register/basic-account');
  })