import { Selector, t } from 'testcafe';
const dataSet = require('resources/users');

class OtpRegistration {
  constructor() {
    this.otpMobileNumber = Selector('[data-unq=auth-otp-input-mobile-number]');
    this.codeSMS = Selector('[data-unq=auth-otp-input-sms-code]');
    this.submitOtp = Selector('[data-unq=auth-otp-button-submit]');
    this.resendOtp = Selector('[data-unq=auth-otp-button-resend-otp]');
  };

  async Otp() {
    await t
      .typeText(this.codeSMS, '111111')
      .click(this.submitOtp);
  };
};

export default new OtpRegistration();