import { Selector, t } from 'testcafe';
import helper from 'lib/helper';

class LoginPage {
  constructor() {
    this.username = Selector('[data-unq=login-input-email]');
    this.password = Selector('[data-unq=login-input-password]');
    this.submitButton = Selector('[data-unq=login-button-submit]');
  };

  async Fologin(email) {
    await t
      .typeText(this.username, email, { paste: true, replace: true })
      .typeText(this.password, helper.getDefaultPassword(), { paste: true, replace: true })
      .click(this.submitButton);
  };
};

export default new LoginPage();