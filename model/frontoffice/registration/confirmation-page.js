import { Selector, t } from 'testcafe';
import { Random } from 'random-js';

class ConfirmationPage {
  constructor() {
    this.startRegistration = Selector('[data-unq=sign-up-complete-button-start-registration]');
  };
};

export default new ConfirmationPage();