import { Selector, t } from 'testcafe';
import { Random } from 'random-js';
const help = require('lib/helper');
class RegisterPage {
  constructor() {
    this.phoneNumber = Selector('[data-unq=register-input-phonenumber]');
    this.referraCode = Selector('[data-unq=register-input-referralcode]');
    this.btnReferral = Selector('[data-unq=button-register-checkReferral]');
    this.termCondition = Selector('[data-unq=register-input-tos]');
    this.termText = Selector('[data-unq=register-input-tosTitle0]')
    this.acceptTermCondition = Selector('[data-unq=button-register-acceptTos]');
    this.btnProcessRegister = Selector('[data-unq=register-button-next]')

    this.pinCode = Selector('[data-unq=pin-input-pincode1]');
    this.btnNextPin = Selector('[data-unq=pin-button-next]');
    this.confrimPincode = Selector('[data-unq=confirm-pin-input-pincode1]');
    this.btnConfirmPin = Selector('[data-unq=confirm-pin-button-next]');

    this.inputOtp = Selector('[data-unq=verification-code-input-otpcode1]')
    this.btnVerifyOtp = Selector('[data-unq=verification-code-button-next]');
    this.ResendOtp = Selector('[data-unq=verification-code-button-resend-otp]');

    this.firstName = Selector('[data-unq=basic-account-input-firstname]');
    this.lastName = Selector('[data-unq=basic-account-input-lastname]');
    this.email = Selector('[data-unq=basic-account-input-email]');
    this.btnNextRegist = Selector('[data-unq=basic-account-button-next]')

    this.address = Selector('[data-unq=address-account-input-address]');
    this.regency = Selector('#__next > div > section > div > div.auth--main-content > div.auth--form.flex.items-center.justify-center > form > div:nth-child(3) > div > div > div.css-1wy0on6 > div');
    this.village = Selector('#__next > div > section > div > div.auth--main-content > div.auth--form.flex.items-center.justify-center > form > div:nth-child(4) > div > div > div.css-1wy0on6 > div');

    this.BirthDate = Selector('div.auth:nth-child(1) section.auth--container div.auth--content div.auth--main-content div.auth--form.flex.items-center.justify-center form.form div:nth-child(2) div.relative.flex.items-center > input.input');
    this.btnNextBirtday = Selector('[data-unq=address-account-button-next]');

    // this.genderFemale = Selector('[data-unq=gender-account-input-gender-female]');
    this.genderMale = Selector('[data-unq=gender-account-input-gender-male]');
    this.btnNextGen = Selector('[data-unq=gender-account-button-next]');

    this.errorMessageregist = Selector('[data-unq=register-input-referral-error]')
    this.successMessageRegist = Selector('[data-unq=register-input-referral-success]')
    this.validationMessage = Selector('div.message--description.error')
    this.alertDanger = Selector('div.alert--title')
    this.alertOtp = Selector('[data-unq=alert-danger]')
  };

  async register(name) {
    await t
      .typeText(this.phoneNumber)
      .typeText(this.referraCode)
      .click(this.termCondition)
      .click(this.btnProcessRegister)

      .click(this.pinCode)
      .typeText(this.confrimPincode)
      .click(this.btnNext)
      .click(this.btnVerifyPin)
      .click(this.updateProfile)
      .typeText(this.email)
      .typeText(this.firstName)
      .typeText(this.lastName)
      .click(this.gender)
      .typeText(this.BirthDate)
  };

  randDate() {
    let randomNumber = new Random().integer(0, 30);
    return Selector(this.expiredDateSelector.replace('()', randomNumber));
  };
  randMonth() {
    let randomNumber = new Random().integer(0, 11);
    return Selector(this.expiredMonthSelector.replace('()', randomNumber));
  };
  randYear() {
    let randomNumber = new Random().integer(1, 50);
    return Selector(this.expiredYearSelector.replace('()', randomNumber));
  };

};

export default new RegisterPage();